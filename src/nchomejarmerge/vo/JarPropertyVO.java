package nchomejarmerge.vo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JarPropertyVO {
	/**
	 * 需要处理的模块名,线程安全
	 */
	private List<String> moduleNames = Collections.synchronizedList(new ArrayList<String>());
	/**
	 * public部分路径
	 */
	private String publicPath;
	/**
	 * client部分路径
	 */
	private String clientPath;
	/**
	 * private部分路径
	 */
	private String privatePath;
	/**
	 * 后缀部分路径
	 */
	private String postfix;
	/**
	 * 删除原有jar
	 */
	private boolean bdeloriginal = false;

	public List<String> getModuleNames() {
		return moduleNames;
	}

	public void setModuleNames(List<String> moduleNames) {
		this.moduleNames = moduleNames;
	}

	public String getPublicPath() {
		return publicPath;
	}

	public void setPublicPath(String publicPath) {
		this.publicPath = publicPath;
	}

	public String getClientPath() {
		return clientPath;
	}

	public void setClientPath(String clientPath) {
		this.clientPath = clientPath;
	}

	public String getPrivatePath() {
		return privatePath;
	}

	public void setPrivatePath(String privatePath) {
		this.privatePath = privatePath;
	}

	public String getPostfix() {
		return postfix;
	}

	public void setPostfix(String postfix) {
		this.postfix = postfix;
	}
	
	/**
	 * 获取一个模块名，并从list中移除,考虑多线程安全，使用synchronized关键字
	 * @return
	 */
	public synchronized String getOneModuleName(){
		if(!this.moduleNames.isEmpty()){
			return this.moduleNames.remove(0);
		}
		return null;
	}
	
	public synchronized boolean existToMergeModule(){
		return !this.moduleNames.isEmpty();
	}
	
	public String toString(){
		return this.clientPath + this.publicPath + this.privatePath + this.postfix + this.moduleNames.size();
	}

	public boolean isBdeloriginal() {
		return bdeloriginal;
	}

	public void setBdeloriginal(boolean bdeloriginal) {
		this.bdeloriginal = bdeloriginal;
	}
	
}
