package nchomejarmerge.main;

import org.apache.log4j.PropertyConfigurator;

import nchomejarmerge.config.ConfigProcessor;
import nchomejarmerge.merge.JarMergeProcessor;
import nchomejarmerge.vo.JarPropertyVO;

public class Mainer {
	
	public static void main(String[] args){
		// 配置log4j 
		PropertyConfigurator.configure(Mainer.class.getResourceAsStream("log4j.properties"));
		ConfigProcessor processor = new ConfigProcessor();
		try{
			// 初始化配置
			processor.initPropertiesFile();
			// 获取配置VO
			JarPropertyVO propertyVO = processor.getPropVO();
			// 根据CPU核心数获取多线程合并jar包
			JarMergeProcessor mergeProcessor = new JarMergeProcessor(propertyVO);
			mergeProcessor.mergeJars();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
	}

}
