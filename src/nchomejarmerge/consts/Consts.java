package nchomejarmerge.consts;

public class Consts {

	public static final String DEFAULT_FILENAME_PROP = "config.properties";
	
	public static final String PROP_PUBLICPATH = "module.publicpath";
	
	public static final String PROP_CLIENTPATH = "module.clientpath";
	
	public static final String PROP_PRIVATEPATH = "module.privatepath";
	
	public static final String PROP_GENJAR_POSTFIX = "genjar.postfix";
	
	public static final String PROP_MODULE_NAMES = "module.names";
	
	public static final String TEMP_FILE_POSTFIX = ".jartmp";
	
	public static final String PROP_DELORIGINAL = "";
	
	public static final String TRUE_STRING = "true";
	
}
