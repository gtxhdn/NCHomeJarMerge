package nchomejarmerge.merge;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import nchomejarmerge.consts.Consts;
import nchomejarmerge.vo.JarPropertyVO;

import org.apache.log4j.Logger;

/**
 * jar合并线程
 * 
 * @author gaotx
 * 
 */
public class JarMergeThread implements Runnable {

	List<String> dirs = new ArrayList<String>();

	private JarPropertyVO propertyVO;

	private String moduleName = null;

	private byte[] buffer = new byte[4096];

	private int bufferSize;

	private static Logger logger = Logger.getLogger(JarMergeThread.class);

	public JarMergeThread(JarPropertyVO propertyVO, String oneModuleName) {
		this.propertyVO = propertyVO;
		this.moduleName = oneModuleName;
	}

	@Override
	public void run() {
		logger.info("线程:" + Thread.currentThread().getName());
		// 如果没有模块名，直接返回
		if (moduleName == null || moduleName.trim().length() == 0) {
			return;
		}
		try {
			logger.info(moduleName + "模块进行处理中....");
			// 0. 处理public文件夹
			logger.info(moduleName + "public文件夹进行处理中....");
			// 公共文件夹路径
			String publicFolderPath = System.getProperty("user.dir") + "/" + moduleName + "/"
					+ this.propertyVO.getPublicPath();
			// 输出all路径
			String outPutPubName = publicFolderPath + "pub" + moduleName + this.propertyVO.getPostfix()
					+ Consts.TEMP_FILE_POSTFIX;
			// 合并jar文件
			mergeJarFiles(publicFolderPath, outPutPubName);

			// 1. 处理client文件夹
			logger.info(moduleName + "client文件夹进行处理中....");
			String clientFolderPath = System.getProperty("user.dir") + "/" + moduleName + "/"
					+ this.propertyVO.getClientPath();
			// 输出all路径
			String outPutClientName = clientFolderPath + "ui" + moduleName + this.propertyVO.getPostfix()
					+ Consts.TEMP_FILE_POSTFIX;
			// 合并jar文件
			mergeJarFiles(clientFolderPath, outPutClientName);

			// 2. 处理private文件夹
			logger.info(moduleName + "private文件夹进行处理中....");
			String privateFolderPath = System.getProperty("user.dir") + "/" + moduleName + "/"
					+ this.propertyVO.getPrivatePath();
			// 输出all路径
			String outPutPrivateName = privateFolderPath + moduleName + this.propertyVO.getPostfix()
					+ Consts.TEMP_FILE_POSTFIX;
			// 合并jar文件
			mergeJarFiles(privateFolderPath, outPutPrivateName);

			logger.info(moduleName + "模块处理完毕");
		} catch (ZipException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	@SuppressWarnings("rawtypes")
	private void mergeJarFiles(String path, String outPutFileName) throws ZipException, IOException {
		// 获取所有jar文件
		List<File> jarFileList = getAllJarFiles(path);
		// 合并到一个jar文件
		Manifest manifest = getManifest();
		FileOutputStream fos = new FileOutputStream(outPutFileName);
		JarOutputStream jOutputStream = null;
		for (int i = 0; i < jarFileList.size(); i++) {
			File aJarFile = jarFileList.get(i);
			logger.info(aJarFile.getAbsolutePath() + "---->" + outPutFileName);
			ZipFile jarFile = new ZipFile(aJarFile);
			Enumeration entities = jarFile.entries();
			while (entities.hasMoreElements()) {
				ZipEntry entry = (ZipEntry) entities.nextElement();
				// 不只是文件夹重复，可能class文件也重复
				if (this.dirs.contains(entry.getName())) {
					continue;
				}
				this.dirs.add(entry.getName());
				if (entry.getName().toLowerCase().startsWith("meta-inf")) {
					continue;
				}
				// 不处理_all.jar的文件
				if (entry.getName().toLowerCase().endsWith("_all.jar")) {
					continue;
				}
				if (jOutputStream == null) {
					jOutputStream = new JarOutputStream(fos, manifest);
				}
				InputStream in = jarFile.getInputStream(jarFile.getEntry(entry.getName()));
				jOutputStream.putNextEntry(new ZipEntry(entry.getName()));

				while ((this.bufferSize = in.read(this.buffer, 0, this.buffer.length)) != -1) {
					jOutputStream.write(this.buffer, 0, this.bufferSize);
				}
				in.close();
				jOutputStream.closeEntry();
			}
			jarFile.close();
			if (aJarFile.isFile() && aJarFile.exists() && this.propertyVO.isBdeloriginal()) {
				if(!aJarFile.delete()){
					logger.info(aJarFile.getAbsolutePath() + "文件删除失败");
				}
			}
		}
		if (jOutputStream != null) {
			jOutputStream.close();

		}
		fos.close();
		// 改名
		File renameFile = new File(outPutFileName);
		String newFileName = outPutFileName.replace(".jartmp", ".jar");
		renameFile.renameTo(new File(newFileName));
	}

	/**
	 * 生成维护信息
	 * 
	 * @return
	 */
	protected Manifest getManifest() {
		Manifest manifest = new Manifest();
		Attributes attribute = manifest.getMainAttributes();
		attribute.putValue("Manifest-Version", "1.0");
		return manifest;
	}

	/**
	 * 获取所有的jar文件
	 * 
	 * @param path
	 * @return
	 */
	private List<File> getAllJarFiles(String path) {
		File dictFile = new File(path);
		File[] files = dictFile.listFiles();
		List<File> jarFileList = new ArrayList<File>();
		if (files == null || files.length == 0) {
			return jarFileList;
		}
		for (File aFile : files) {
			if (aFile.isFile() && aFile.getName().endsWith(".jar")) {
				jarFileList.add(aFile);
			}
		}
		return jarFileList;
	}
}
