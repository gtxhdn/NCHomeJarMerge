package nchomejarmerge.merge;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import nchomejarmerge.vo.JarPropertyVO;

import org.apache.log4j.Logger;

public class JarMergeProcessor {

	private JarPropertyVO propertyVO = null;

	private static Logger logger = Logger.getLogger(JarMergeProcessor.class);

	public JarMergeProcessor(JarPropertyVO propertyVO) {
		this.propertyVO = propertyVO;
	}

	public void mergeJars() {
		int coreNums = Runtime.getRuntime().availableProcessors();
		logger.info("CPU核心数:" + coreNums);
		// 固定线程池
		ExecutorService executorService = Executors.newFixedThreadPool(coreNums);
		// 批量处理模块设置
		while (propertyVO.existToMergeModule()) {
			String oneModuleName = propertyVO.getOneModuleName();
			JarMergeThread aThread = new JarMergeThread(propertyVO, oneModuleName);
			executorService.execute(aThread);
		}
		executorService.shutdown();
	}

}
