package nchomejarmerge.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import nchomejarmerge.consts.Consts;
import nchomejarmerge.vo.JarPropertyVO;

import org.apache.log4j.Logger;


public class ConfigProcessor {
	
	private static Logger logger = Logger.getLogger(ConfigProcessor.class);  

	private JarPropertyVO propVO = new JarPropertyVO();

	/**
	 * 获取当前运行路径下的properties文件，如果没有则将jar包中默认properties复制到运行路径下，然后用该路径下properties
	 * 
	 * @throws IOException
	 */
	public void initPropertiesFile() throws IOException {
		String defaultConfigFilePath = System.getProperty("user.dir") + "/"
				+ Consts.DEFAULT_FILENAME_PROP;
		logger.info("配置文件地址:" + defaultConfigFilePath);
		File configFile = new File(defaultConfigFilePath);
		// 如果文件不存在，拷贝默认的属性文件
		if (!configFile.exists()) {
			logger.info("初始化配置文件......");
			copyDefaultPropertyFile(configFile);
		}
		// 读取配置文件
		logger.info("读取配置文件......");
		readExistProperies(configFile);
	}

	private void copyDefaultPropertyFile(File configFile) throws IOException {
		InputStream is = ConfigProcessor.class
				.getResourceAsStream(Consts.DEFAULT_FILENAME_PROP);
		FileOutputStream os = new FileOutputStream(configFile);
		int bytesRead;
		byte[] buf = new byte[4 * 1024]; // 4K
		while ((bytesRead = is.read(buf)) != -1) {
			os.write(buf, 0, bytesRead);
		}
		os.flush();
		os.close();
		is.close();
	}

	/**
	 * 读取已存在的properties
	 * 
	 * @param configFile
	 * @throws IOException
	 */
	private void readExistProperies(File configFile) throws IOException {
		FileInputStream is = new FileInputStream(configFile);
		Properties prop = new Properties();
		prop.load(is);
		if (is != null) {
			is.close();
		}
		setValue(prop);
	}

	private void setValue(Properties prop) {
		this.propVO.setClientPath(prop.getProperty(Consts.PROP_CLIENTPATH));
		this.propVO.setPublicPath(prop.getProperty(Consts.PROP_PUBLICPATH));
		this.propVO.setPrivatePath(prop.getProperty(Consts.PROP_PRIVATEPATH));
		this.propVO.setPostfix(prop.getProperty(Consts.PROP_GENJAR_POSTFIX));
		String[] moduleNames = null;
		if(prop.getProperty(Consts.PROP_MODULE_NAMES) != null && prop.getProperty(Consts.PROP_MODULE_NAMES).trim().length() > 0){
			moduleNames = prop.getProperty(Consts.PROP_MODULE_NAMES).split(",");
			this.propVO.setModuleNames(new ArrayList<String>(Arrays.asList(moduleNames)));
		}
		String bdeloriginal = prop.getProperty(Consts.PROP_DELORIGINAL);
		if(bdeloriginal != null && Consts.TRUE_STRING.equals(bdeloriginal.toLowerCase())){
			this.propVO.setBdeloriginal(true);
		}
		//如果为空，则获取当前Modules下全部模块
		setAllModuleNames();
	}

	private void setAllModuleNames() {
		if(this.propVO.getModuleNames().isEmpty()){
			List<String> allModuleList = new ArrayList<String>();
			File moduleFolder = new File(System.getProperty("user.dir"));
			File[] allFiles = moduleFolder.listFiles();
			if(allFiles != null && allFiles.length > 0){
				for(File aFile : allFiles){
					if(aFile.isDirectory()){
						allModuleList.add(aFile.getName());
					}
				}
			}
			this.propVO.setModuleNames(allModuleList);
		}
	}

	public JarPropertyVO getPropVO() {
		return propVO;
	}

	public void setPropVO(JarPropertyVO propVO) {
		this.propVO = propVO;
	}
}
